package com.prasanth.training.RestTemplate.controller;

import com.prasanth.training.RestTemplate.pojos.Person;
import com.prasanth.training.RestTemplate.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/p")
public class HomeController {

    @Autowired
    private PersonService personService;

    @RequestMapping(value="/getPersons", method = RequestMethod.GET)
    public List<Person> getAllPersons(){
        return personService.getAllPersons();
    }
    @RequestMapping(value="/getPerson/{id}", method = RequestMethod.GET)
    public Person getAllPersons(@PathVariable int id){
        return personService.getAllPersonById(id);
    }

    @RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
    public String deletePerson(@PathVariable int id){
        return personService.deletePersonById(id);
    }

    @RequestMapping(value="/updatePerson", method = RequestMethod.PUT)
    public String getAllPersons(@RequestBody Person person){
        return personService.updatePerson(person);
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addPerson(@RequestBody Person person){
        return personService.addPerson(person);
    }
}
